import { NgModule } from '@angular/core';

import { AppDashboardRoutingModule } from './app-dashboard-routing.module';
import { AppDashboardComponent } from './app-dashboard.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule,
    AppDashboardRoutingModule
  ],
  declarations: [AppDashboardComponent],
  bootstrap: [AppDashboardComponent]
})
export class AppDashboardModule { }
