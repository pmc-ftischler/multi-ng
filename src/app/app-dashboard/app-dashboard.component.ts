import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dashboard-root',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
