import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppOneRoutingModule } from './app-one-routing.module';

import { AppOneComponent } from './app-one.component';


@NgModule({
  declarations: [
    AppOneComponent
  ],
  imports: [
    BrowserModule,
    AppOneRoutingModule
  ],
  providers: [],
  bootstrap: [AppOneComponent]
})
export class AppOneModule { }
