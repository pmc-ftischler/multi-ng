import { Component } from '@angular/core';

@Component({
  selector: 'app-one-root',
  templateUrl: './app-one.component.html',
  styleUrls: ['./app-one.component.scss']
})
export class AppOneComponent {
  title = 'app one';
}
