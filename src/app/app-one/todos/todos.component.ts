import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-one-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TodosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
