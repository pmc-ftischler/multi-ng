import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppTwoRoutingModule } from './app-two-routing.module';

import { AppTwoComponent } from './app-two.component';


@NgModule({
  declarations: [
    AppTwoComponent
  ],
  imports: [
    BrowserModule,
    AppTwoRoutingModule
  ],
  providers: [],
  bootstrap: [AppTwoComponent]
})
export class AppTwoModule { }
