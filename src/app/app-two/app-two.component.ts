import { Component } from '@angular/core';

@Component({
  selector: 'app-two-root',
  templateUrl: './app-two.component.html',
  styleUrls: ['./app-two.component.scss']
})
export class AppTwoComponent {
  title = 'app two';
}
