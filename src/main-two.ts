import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './two/environments/environment';
import { AppTwoModule } from './app/app-two/app-two.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppTwoModule)
  .catch(err => console.log(err));
