import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './one/environments/environment';
import { AppOneModule } from './app/app-one/app-one.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppOneModule)
  .catch(err => console.log(err));
