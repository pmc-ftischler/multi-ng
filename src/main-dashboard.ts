import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './dashboard/environments/environment';
import { AppDashboardModule } from './app/app-dashboard/app-dashboard.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppDashboardModule)
  .catch(err => console.log(err));
